﻿using System;

namespace comp5002_10011751_assessment01
{
    class Program
    {
        
        static void Main(string[] args)
        {
            // declaring variables
            var name1 = "";
            var num = 0.00;
            var y = ""; 
        
            Console.WriteLine("***********************************************************************************");
            Console.WriteLine("**********************      Welcome To Mantis Clothing      ***********************");
            Console.WriteLine("***********************************************************************************");
            // Greeting the customer
            Console.WriteLine("-----------------------------------------------------------------------------------");
            Console.WriteLine("***************************  Please Enter Your Name  ******************************");
                  name1 = Console.ReadLine();
            // asking the user for their name 
            Console.WriteLine("***********************************************************************************");
            Console.WriteLine($"****************************  Thank you {name1} **********************************");
            Console.WriteLine("***********************************************************************************");
            Console.WriteLine("************************  Please Enter a 2 Decimal Number  ************************");
                 num = Double.Parse(Console.ReadLine());
            // asking the user for a 2 decimal number 
            Console.WriteLine("***********************************************************************************");
            Console.WriteLine("*******************  Do you want to add another Number Y/N  ***********************");
            Console.WriteLine("***********************************************************************************");
            // asking the user a Y/N question
            Console.WriteLine("------------------------------ Y = Yes / N = No -----------------------------------");
            Console.WriteLine("***********************************************************************************"); 
            // declaring Y for yes and N for no 
               

                y = Console.ReadLine();
            if(y == "y")
            {
                Console.WriteLine("********************  Please Choose another 2 Decimal Number  *********************");
                num += double.Parse(Console.ReadLine());
                Console.WriteLine($"******************  Your total is {num*1.15:C2} Including GST  *******************");  
            }
            else if(y == "n" )
            {
                Console.WriteLine();
                Console.WriteLine($"******************  Your total is {num*1.15:C2} Including GST  *******************");
            }
            // if the user types in Y, add previous 2 decimal number with new 2 decimal number entered and add GST  
            // if the user types in N, use first 2 decimal number and add GST  
            Console.WriteLine("-----------------------------------------------------------------------------------");
            Console.WriteLine("***********************************************************************************");
            Console.WriteLine("**************  Thank You for Shopping with us, Please come again  ****************");
            Console.WriteLine("***********************************************************************************");
            Console.WriteLine("-----------------------------------------------------------------------------------");
            Console.WriteLine("********************  Press <ENTER> to Close the Application  *********************");
            Console.WriteLine("-----------------------------------------------------------------------------------");
            // Thanking your user for shopping and asking for them to come back
            // Asking the user to Press Enter to close the application

            ConsoleKeyInfo keyInfo;
                while(keyInfo.Key != ConsoleKey.Enter)
            keyInfo = Console.ReadKey();
            // the application will only close when <ENTER> is pressed 
        }
    }
}